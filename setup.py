from distutils.core import setup

from setuptools import find_packages

setup(
    name='OLX test',
    packages=find_packages(exclude=['tests']),
    version='1.0',
    description='OLX test',
    author='Michal Wachowski',
    author_email='wachowski.michal@gmail.com',
    install_requires=[],
    test_suite='tests',
    tests_require=[
        'flake8',
        'pytest'
    ]
)
