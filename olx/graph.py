from abc import ABC, abstractmethod
from typing import NamedTuple, Optional, Set, List, Dict


class State(NamedTuple):
    name: str
    transitions: Optional[Set[str]] = set()


class Payload(ABC):
    @abstractmethod
    def current_state(self) -> str:
        ...

    @abstractmethod
    def transit_state(self, state: str) -> None:
        ...


class GraphException(Exception):
    @classmethod
    def invalid_state(cls, state: str):
        return cls('Invalid state %s' % state)

    @classmethod
    def invalid_transition(cls, from_state: str, to_state: str):
        return cls('Invalid transition from %s to %s' % (from_state, to_state))


class Graph:
    def __init__(self, states: List[State]) -> None:
        self.__graph: Dict[str, State] = {
            state.name: state
            for state in states
        }

    def transit(self, entity: Payload, to: str) -> None:
        try:
            state = self.__graph[entity.current_state()]
        except KeyError:
            raise GraphException.invalid_state(entity.current_state())

        if to not in state.transitions:
            raise GraphException.invalid_transition(state.name, to)

        entity.transit_state(to)
