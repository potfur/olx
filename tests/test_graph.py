from pytest import raises

from olx.graph import Graph, State, Payload, GraphException


class TransitionalPayload(Payload):
    def __init__(self, current: str) -> None:
        self.states = [current]

    def current_state(self) -> str:
        return self.states[-1:][0]

    def transit_state(self, state: str) -> None:
        self.states.append(state)


class TestGraph:
    def setup_method(self):
        states = [
            State('new', {'limited', 'active'}),
            State('limited', {'active'}),
            State('active', {'outdated'}),
            State('outdated', {'active', 'removed'}),
            State('removed'),
        ]

        self.graph = Graph(states)

    def test_it_will_raise_exception_when_payload_is_in_unknown_state(self):
        entity = TransitionalPayload('unknown')

        with raises(GraphException) as e:
            self.graph.transit(entity, 'limited')

        assert str(e.value) == str(GraphException.invalid_state('unknown'))

    def test_it_will_raise_exception_when_transition_is_unknown(self):
        entity = TransitionalPayload('new')

        with raises(GraphException) as e:
            self.graph.transit(entity, 'unknown')

        assert str(e.value) == str(
            GraphException.invalid_transition('new', 'unknown')
        )

    def test_it_will_change_payload_state_on_valid_transition(self):
        entity = TransitionalPayload('new')
        self.graph.transit(entity, 'limited')

        assert entity.states == [
            'new',
            'limited'
        ]

    def test_allows_for_loops(self):
        entity = TransitionalPayload('active')
        self.graph.transit(entity, 'outdated')
        self.graph.transit(entity, 'active')
        self.graph.transit(entity, 'outdated')

        assert entity.states == [
            'active',
            'outdated',
            'active',
            'outdated'
        ]

    def test_allows_for_blind_state(self):
        entity = TransitionalPayload('removed')

        with raises(GraphException) as e:
            self.graph.transit(entity, 'removed')

        assert str(e.value) == str(
            GraphException.invalid_transition('removed', 'removed')
        )
