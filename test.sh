#!/bin/sh

check () { echo $@; $@ || exit $?; }

check flake8
check pytest tests/unit -xv
check pytest tests/integration -xv
check pytest tests/functional -xv